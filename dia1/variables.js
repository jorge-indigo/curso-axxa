// Usando la palabra reservada var

var x = 0;
x = true;
x = "Hola Mundo";
x = undefined;

// Usando la palabra reservada let

let y = 0;

// Usando la palabra reservada const

const z = 0;


// Diferencias
// Usando var
// 1. Puedo asignar valores mas de una vez
var x1 = 0;
x1 = true;

// 2. Puedo volver a declararla
var x2 = 0;
var x2 = "Hola";

// 3. La variable vive dentro de la función más cercana


// Usando let
// 1. Puedo asignar valores mas de una vez
let y1 = 0;
y1 = true;

// 2. No puedo volver a declarar la variable
let y2 = 0;
//let y2 = true;

// 3. La variable vive en las llaves mas cercanas

// Usando const

// 1. Lo mismo que let
const z1 = 0;
//z1 = true;
