/*
    number
    string
    boolean
    null
    undefined
    Array
    Object
    Map
    Set
*/

let numerico = 0;

let booleano = true;

let cadena = "esto es una cadena";
let cadena2 = 'esto tambien es una cadena';
let cadena3 = `el valor de la cadena es ${cadena}`; //"cadena es =>" + cadena;

let indefinido1;
let indefinido2 = undefined;

let nulo = null;

//let arreglo = new Array();
let arreglo = []; 

// let objeto = new Object();
let objeto = {};

let mapa = new Map();
mapa.set("algo", []);

console.log("Mapa: ", mapa);

let conjunto = new Set();

conjunto.add(0);
conjunto.add(1);
conjunto.add(0);

console.log("Conjunto: ", conjunto);