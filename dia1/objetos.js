"use strict";

let objeto = {};

objeto.nuevaPropiedad = "algo";
objeto.otraPropiedad = 0;

delete objeto.nuevaPropiedad;

objeto["otraPropiedadMas"] = [];

let atributo = "ultimointento";
objeto[atributo] = false;

console.log("Mi objeto", objeto);

// Persona

var coso = "_1";

let persona = {
    nombre: "Juan",
    edad: 32,
    esFumador: true,
    esAlcoholico: true,
    direccion: {
        calle: "Felix Cuevas",
        numExt: 366,
        colonia: "Del Valle",
        pais: {
            codigo: "MX",
            nombre: "México"
        }
    },

    [ "coso" + coso ]: false,

    beber() {
        console.log("glug glug glug...");
    },

    mori() {
        console.log("x.x");
    },

    saluda(nombre) {
        console.log("Hola,", nombre);
    }
}

console.log("Persona", persona);
console.log("La persona vive en", persona.direccion.pais.nombre);


for(let atributo in persona) {
    console.log("Atributo de persona:", atributo);
}

persona.beber();
persona.mori();
persona.saluda("Jorge");

// Shorthand
let nombre = "Jorge";
let edad = 24;

let persona2 = { nombre, edad, esFumador: false };
console.log("La otra persona", persona2);