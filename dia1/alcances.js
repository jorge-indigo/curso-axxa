/* Fuga de memoria con var

var x = 0;

if (true) {
    var x2 = 0;

    // Operar con x2
}

console.log("El valor de x es", x);
console.log("El valor de x2 es", x2);*/

let x = 0;

if (true) {
    let x2 = 0;

    // Operar con x2
}

console.log("El valor de x es", x);
console.log("El valor de x2 es", x2);