// Destructuring usando arreglos

var a = 0;
var b = 1;

[ b, a ] = [a, b];

console.log("a =", a, ", b =", b);


// Destructuring de propiedades

var carro = { llantas: 4, modelo: "Accord", marca: "Honda", color: "Rojo" };

/*let llantas = carro.llantas;
let modelo = carro.modelo;
let marca = carro.marca;*/

let { llantas, modelo, marca } = carro;
console.log("Caracteristicas del carro", llantas, modelo, marca);