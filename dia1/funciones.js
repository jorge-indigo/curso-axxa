function miFuncion(p1, p2, p3) {
    //return null;
}

function suma(a, b) { return a + b };

console.log("Suma de 10 + 20 = ", suma(10, 20));

let multiplicacion = function (a, b) { return a * b };

console.log("Multiplicacion de 8 x 7 = ", multiplicacion(8,7));

function regresandoUnaFuncion() {
    return function () {
        console.log("Función dentro de una función");
    }
}

regresandoUnaFuncion()();

function recibirUnaFunction(f) {
    f();
}

recibirUnaFunction(function () {
    console.log("Estoy ejecutando una funcion dentro de una funcion")
});


// Parametros destructuring
function imprimePersona({ nombre, edad }, algo = null) {
    console.log(nombre, edad, algo);
}


let p = { nombre: "X",  esFumador: true, edad: 20 }
imprimePersona(p, 0);