// 1. Importar http usando CommonJS (Node, Webpack, AMD, Browserify, Require, ES6)
var http = require("http");
var chalk = require('chalk');
var config = require("./config");

// 2. Instanciar el servidor
var servidor = http.createServer(function (request, response) {

    let url = request.url;

    console.log(chalk.red('Hello', chalk.underline.bgBlue('world') + '!'));

    switch(url) {
        case config.URL:
            response.end("Hello World");
        default:
            response.end("Denegado");
    }
});

// 3. Escuchar el puerto 9000
servidor.listen(config.PORT, function() { console.log("Escuchando en el puerto " + config.PORT) });