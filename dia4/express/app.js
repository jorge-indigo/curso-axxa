// 1. Importar express
var express = require("express");
var bodyParser = require("body-parser");

// 2. Instanciar un servidor
var app = express();

// Configuración
app.use(bodyParser.json());

// 3. Agregar una ruta
app.get("/", function(request, response) {
    response.end("Hello World");
});

app.get("/test", function(request,response) {
    response.end("Testing");
});

// Parametros por QueryString
app.get("/test/query", function(request, response) {
    console.log("QueryString", request.query);
    response.end();
});

app.get("/test/:id/params", function(request, response){
    console.log("URL Params", request.params);
    response.end();
});

app.post("/test", function(request, response) {
    console.log("Body", request.body);
    response.end("Testing POST");
});

// 4. Escuchar un puerto
app.listen(8080, function () { console.log("Escuchando puerto 8080") });