var express = require("express");

var app = express();

// Middlewares
app.use("/awesome", function(request, response, next) {
    if(request.query.session == 0) {
        next();
    } else {
        response.status(404).end("NOT FOUND");
    }
});

app.use("/awesome/:name", function(request, response, next) {
    // Si el nombre es ema => next();
    // Si no => response.status(404).end("YOUR NOT EMMA");
    if(request.params.name == "ema"){
        next();
    }else{
        response.status(404).end("YOUR NO EMA");
    }
});

// Rutas
app.get("/", function(request,response) {
    response.end("Hello! You have session");
});

app.get("/axa", function(request,response) {
    response.end("Axa :(, nos explotan, AYUDA!!!");
});

app.get("/awesome", function(request,response) {
    response.end("Hello! You have a session and you are awesome");
});

app.get("/awesome/:name", function(request, response) {
    response.end(`Hello ${ request.params.name }! You are awesome`); 
});

app.listen(8081);