var express = require("express");
var app = express();

var bodyParser = require("body-parser");

require("./config/database");

app.use(bodyParser.json());

let LoginRouter = require("./routes/login");
app.use(LoginRouter);

let UserRouter = require("./routes/user");
app.use(UserRouter);

app.listen(9080, function () {
    console.log("Ejecutando en el puerto 9080");
});