var express = require("express");
var Router = express.Router();
var UserModel = require("../schemas/user");
var jwt = require("jsonwebtoken");

Router.post("/login", function (request, response) {

    let user = new UserModel({
        username: request.body.username,
        password: request.body.password
    });

    user.authenticate(function (err, isMatch, databaseUser) {
        if (err) return response.status(400).end();

        if (isMatch) {
            let token = jwt.sign(databaseUser, global.JWT_SECRET);
            response.status(200).json({ token });
        } else {
            response.status(401).json({ error: "No autorizado" });
        }
    });
});

Router.get("/login/test", function (request, response) {
    response.status(200).end("Hello World");
});

module.exports = Router;
