var express = require("express");
var Router = express.Router();
var jwt = require("jsonwebtoken");

var UserModel = require("../schemas/user");

Router.use("/users", function (request, response, next) {
    let token = request.headers["x-auth-token"];

    jwt.verify(token, global.JWT_SECRET, function (err, user) {
        if (err) return response.status(401).json({ error: "No autorizado "});
        if (user) return next();
        else return response.status(401).json({ error: "No autorizado "});
    });
});

Router.get("/users", function (request, response) {
    UserModel.find({}, function(err, users) {
        if (err) response.status(400).json({ err });
        else response.status(200).json({ users });
    });
});

module.exports = Router;