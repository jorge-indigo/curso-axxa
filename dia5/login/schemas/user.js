var mongoose = require("mongoose");
var bcrypt = require("bcrypt");

let UserSchema = mongoose.Schema({
    name: String,
    age: Number,
    active: Boolean,
    username: String,
    password: String
});

UserSchema.pre('save', function (next) {
    let user = this;

    bcrypt.genSalt(global.SALT_FACTOR, function (err, salt) {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);

            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.authenticate = function (callback) {
    this.model('User')
        .findOne({ username: this.username }, (err, user) => {
            if (err) return callback(err);
            if (!user) return callback(null, false);

            bcrypt.compare(this.password, user.password, 
                function (err, isMatch) {
                    if (err) return callback(err);

                    callback(null, isMatch, user);
                });
    });
}

module.exports = mongoose.model("User", UserSchema);