var mongoose = require("mongoose");

// Por default toma el puerto 27017
global.db = mongoose.connect("mongodb://localhost/login");

// Valor utilizado para generar contraseñas
global.SALT_FACTOR = 10;

// Llave para generar JWT
global.JWT_SECRET = 'Shhhhhhhh';