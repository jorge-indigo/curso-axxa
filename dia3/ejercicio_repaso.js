// Extender el tipo Object con una función que diga cuándo un objeto es vacío
// {}.isEmpty() => true
// { something: 0 }.isEmpty() => false

Object.prototype.isEmpty = function () {
	let objeto = this
	for(var prop in objeto) {
        if(this.hasOwnProperty(prop))
            return false;
    }
    return true;
}

let test1 = {};
console.log(JSON.stringify(test1), "is empty?", test1.isEmpty());
console.assert(test1.isEmpty());

let test2 = { something: 0, a: "yay" };
console.log(JSON.stringify(test2), "is empty?", test2.isEmpty());
console.assert(!test2.isEmpty());


