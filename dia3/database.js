function WebService() {
    this.animales = []; // { id: Number, nombre: String }
}

// Callback
WebService.prototype.obtenerTodos = function (f) {
    setTimeout(() => {
        console.info("Ejecutando obtenerTodos")
        f(this.animales);
    }, 5000);
}

WebService.prototype.guardar = function (animal, f) {
    setTimeout( () => {
        console.info("Ejecutando guardar");
        this.animales.push(animal);
        f();
    }, 1000);
}

WebService.prototype.eliminar = function (id, f) {
    setTimeout( () => {
        console.info("Ejecutando eliminar")
        for(let indice in this.animales) {
            if (this.animales[indice].id == id) {
                this.animales.splice(indice, 1);
                break;
            }
        }

        f();
    }, 500);
}

WebService.prototype.contar = function (f) {
    setTimeout( () => {
        console.info("Ejecutando contar")
        f(this.animales.length);
    }, 5000);
}

let ws = new WebService();

/* Ejemplo síncrono
let animales = ws.obtenerTodos();
console.assert(animales.length == 0, "En la base no hay animales");

ws.guardar({ id: 0, nombre: "Perro" });

let cuantos = ws.contar();
console.assert(cuantos == 1, "Debe haber 1 animal en la base");

ws.guardar({ id: 1, nombre: "Gato" });

ws.eliminar(0);

let cuantosOtraVez = ws.contar();
console.assert(cuantosOtraVez == 1, "Debe haber 1 animal por que se elimino el Perro");*/

/*ws.obtenerTodos(function (animales) {
    console.log("Animales en la base:", animales);

    ws.guardar({ id: 0, nombre: "Perro" }, function () {
        console.log("Hemos guardado al perro");

        ws.contar(function (cuantos) {
            console.log("Hay", cuantos, "en la base");
        });
    });
});*/

/*ws.obtenerTodos(() => {});
ws.guardar({ id: 0, nombre: "Perro" }, () => {});
ws.contar(() => {});*/

// Callback Hell
ws.guardar({ id: 0, nombre: "Leon" }, function () {
    console.log("Hemos guardado al Leon");

    ws.guardar({ id: 1, nombre: "Pinguino"}, function () {
        console.log("Hemos guardado al Pinguino");

        ws.obtenerTodos(function (animales) {
            console.log("Animales en la base:", animales);

            ws.guardar({ id: 2, nombre: "Oso"}, function () {
                console.log("Hemos guardado al Oso");

                ws.contar(function (cuantos) {
                    console.log("Hay", cuantos, "en la base");

                    ws.eliminar(0, function () {
                        console.log("Hemos eliminado al Oso");
                        
                        ws.obtenerTodos(function (animales) {
                            console.log("Animales en la base:", animales);
                        });
                    });
                });
            });
        });
    });
});